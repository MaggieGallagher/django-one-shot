from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, CreateItemForm
# Create your views here.

def todo_list_list(request):
    lists = TodoList.objects.all()
    context = {
        "todo_list_list": lists,
    }
    return render(request, "todos/list.html", context)

def todo_list_detail(request, id):
    list = TodoList.objects.get(id=id)
    context = {
        "todo_list_detail": list,
    }

    return render(request, "todos/detail.html", context)

def todo_list_create(request):
    if request.method =="POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm()
    context = {
        "todolist_form": form,
    }
    return render(request, "todos/create.html", context)

def todo_list_update(request, id):
    list = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm(instance=list)
    context = {
        "todolist_form": form,
    }
    return render(request, "todos/update.html", context)

def todo_list_delete(request, id):
  list = TodoList.objects.get(id=id)
  if request.method == "POST":
    list.delete()
    return redirect("todo_list_list")

  return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method =="POST":
        form = CreateItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = CreateItemForm()
    context = {
        "createitem_form": form,
    }
    return render(request, "todos/create_item.html", context)

def todo_item_update(request, id):
    item = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = CreateItemForm(request.POST, instance=item)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = CreateItemForm(instance=item)
    context = {
        "createitem_form": form,
    }
    return render(request, "todos/update_item.html", context)
